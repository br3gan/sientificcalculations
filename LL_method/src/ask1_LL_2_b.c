// TODO epafksimenos pinakas

#include <stdio.h>
#include <math.h>

#define NAME_LENGTH 10
#define MAX_RAND 500

int N;

void printMatrix_2(double A[][N], char name[NAME_LENGTH]){
     int i,j;
     printf("Matrix %s:\n",name);
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               printf("%15.15f ", A[i][j]);
          }
          printf("\n");
     }
     printf("\n");
     return;
}

void printMatrix_1(double A[N], char name[NAME_LENGTH]){
     int i;
     printf("Matrix %s:\n",name);
     for (i=0; i<N; i++){
          printf("%15.15f ", A[i]);
     }
     printf("\n\n");
     return;
}

void printMatrix_2_reverse(double A[][N], char name[NAME_LENGTH]){
     int i,j;
     printf("%s Matrix:\n",name);
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               printf("%15.15f ", A[j][i]);
          }
          printf("\n");
     }
     printf("\n");
     return;
}

void initializeArray_1(double A[N]){
     int i;
     for(i=0; i<N; i++){
          A[i]=0;
     }
     return;
}

void initializeArray_2(double A[][N]){
     int i,j;
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               A[i][j]=0;
          }
     }
     return;
}

void findLcolumn(double A[][N], double U[][N], double L[][N], double S[N], int r){
     int p=0, j=0;
     double sum;
     for(p=r+1; p<N; p++){
          L[p][r]=S[p]/U[r][r];
     }
}

int findL(double A[][N], double L[][N]){
     
     if (A[0][0] <= 0)
          return -1;
     L[0][0] = sqrt(A[0][0]);
     int i,j,r,p;
     double sum;
     for (j=1; j<N; j++){
          L[j][0]=A[j][0]/L[0][0];
     }
     for (r=1; r<N-1; r++){
          sum=0.0;
          for (j=0; j<r; j++){
               sum+=L[r][j]*L[r][j];
          }
          if (sum > A[r][r])
               return -1;
          L[r][r]= sqrt( A[r][r] - sum );
          for (p=r+1; p < N; p++){
               sum=0;
               for( j=0; j<r; j++ ){
                    sum+=L[p][j]*L[r][j];
               }
               L[p][r]=(1/L[r][r])*( A[p][r] - sum ); 
          }
     }
     sum=0;
     for (j=0; j<N-1; j++){
          sum+=L[N-1][j]*L[N-1][j];
     }
     if (A[N-1][N-1] < sum )
          return -1;
     L[N-1][N-1] = sqrt( A[N-1][N-1] - sum );
     return 0;
}

void findY(double L[][N], double Y[N], double B[N]){
     int i,j;
     double quantity;
     for (i=0; i<N; i++){
          quantity=0;
          for(j=0; j<i; j++){
               if(i==j)
                    quantity+=Y[j];
               else
                    quantity+=L[i][j]*Y[j];
          }
          Y[i]=(B[i]-quantity)/L[i][i];
     }
     return;
}

void findX(double Lt[][N], double X[N], double Y[N]){
     int i,j;
     double quantity;
     for (i=N-1; i>=0; i--){
          quantity=0;
          for(j=N-1; j>i; j--){
               quantity+=Lt[i][j]*X[j];
          }
          X[i]=(Y[i]-quantity)/Lt[i][i] ;
     }
     return;
}

int main(int argc, char* argv[]){
     
     int readFromFile = 0;
     int readFromUser = 0;
     int random = 0;
     FILE *myfile;
     
     if (argc > 1){
          if ( strcmp(argv[1],"--user") == 0 || strcmp(argv[1],"-u") == 0 )
               readFromUser = 1;
          else if ( strcmp(argv[1],"--file") == 0 || strcmp(argv[1],"-f") == 0 )
               readFromFile = 1;
          else if ( strcmp(argv[1],"--random") == 0 || strcmp(argv[1],"-r") == 0 )
               random = 1;
          else{
               printf("Invalid arguments\n");
               return -1;
          }
     }
     else{
          printf("Please give an option\n");
          printf("--file / -f [path/to/file] : to read from file\n");
          printf("--user / -u : to read from user\n");
          printf("--random / -r [dimension] : to fill a random Matrix\n");
          return 0;
     }
     
     if(readFromFile){
          myfile=fopen(argv[2], "r");
          if (myfile == NULL){
               printf("Give file path\n");
               return -1;
          }
          fscanf(myfile,"%d",&N);
     }
     else if(readFromUser){
          printf("Give Dimension : ");
          scanf("%d",&N);
     }
     else if(random){
          if (argc<3){
               printf("Give Dimension : ");
               scanf("%d",&N);
          }
          else{
               N=atoi(argv[2]);
               printf("Dimension : %d\n", N);
          }
     }
     if (random){
          srand(time(NULL));
     }
     double A[N][N];
     int i;
     int j;
     double myvariable;
     
     if (readFromUser){
          printf("Give A matrix index\n");
     }

     for(i = 0; i < N; i++){
          for (j = 0 ; j < N; j++){
               if(readFromFile)
                    fscanf(myfile,"%lf",&myvariable);
               else if(readFromUser){
                    printf(" A[%d][%d] = ",i,j);
                    scanf("%lf",&myvariable);
                    if (j==N-1) printf("\n");
               }
               else if(random){
                    //myvariable = (double)(rand()%MAX_RAND);
                    if (i==j)
                         myvariable = N;
                    else
                         myvariable = 1.0/(i+j+1);
               }
               A[i][j]=myvariable;
          }
     }
     
     if(readFromFile)
          fclose(myfile);
     
     double L[N][N];
     double Lt[N][N];
     double X[N];
     double Y[N];
     
     printMatrix_2(A,"A");
     
     initializeArray_2(L);
     initializeArray_1(X);
     initializeArray_1(Y);
     
     if ( findL(A,L) < 0 ){
          printf("Not a symmetric positive definite matrix.\n");
          return 0;
     }
     
     for (i=0; i<N; i++){
          for (j=0; j<N; j++){
               Lt[j][i]=L[i][j];
          }
     }
     
     printMatrix_2(L,"L");
     printMatrix_2(Lt,"Lt");     
     
     double I[N][N];
     double Inverse[N][N];
     
     initializeArray_2(I);
     for (i=0; i<N; i++){
          I[i][i]=1;
     }
     
     for (i=0; i<N; i++){
          findY(L,Y,I[i]);
          findX(Lt,Inverse[i],Y);
     }
     
     printMatrix_2_reverse(Inverse,"Inverse");
     
     return 0;
}