#include <stdio.h>

#define NAME_LENGTH 10

/**
 * Global value for the Dimension of the Matrix
 */
int N;

/**
 * @brief Prints out a two-dimensioned Matrix.
 * @param A The matrix which is going to be printed.
 * @param name The name of the matrix for elegand output.       
 */
void printMatrix_2d(double A[][N], char name[NAME_LENGTH]){
     int i,j;
     printf("Matrix %s:\n",name);
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               printf("%3.3f ", A[i][j]);
          }
          printf("\n");
     }
     printf("\n");
     return;
}

/**
 * @brief Prints a one-dimensioned Matrix.
 * @param A The matrix which is going to be printed.
 * @param name The name of the matrix for elegand output.
 */
void printMatrix_1d(double A[N], char name[NAME_LENGTH]){
     int i;
     printf("Matrix %s:\n",name);
     for (i=0; i<N; i++){
          printf("%3.3f ", A[i]);
     }
     printf("\n\n");
     return;
}

/**
 * @brief Updates a row of U matrix.
 * @param U States the U matrix of the LU method.
 * @param row States the row of the U matrix which is going to change.
 * @param from States the column of the first new calculated number (where the new zero is going).
 * @param multiplier States the multiplier which is zero-ing the first element of the given row.      
 */
void updateUrow(double U[][N], int row, int from, double multiplier){
     int i=0;
     U[row][from]=0;
     for (i=from+1; i<N; i++){
          U[row][i] = U[row][i] + U[from][i]*multiplier;
     }
     return;
}

/** 
 * @brief Swaps two lines of a matrix.
 * @param U the matrix which lanes will get swapped.
 * @param line the line which will get swapped.
 * @param guideLine the new guide line
 * 
 */
void swapLines(double U[][N], int line, int guideLine){
     int i=0;
     double tmp;
     for (i=line ; i<N; i++){
          tmp=U[line][i];
          U[line][i]=U[guideLine][i];
          U[guideLine][i]=tmp;
     }
     return;
}

void swapElements(double B[N], int line, int guideLine){
     double tmp; 
     tmp=B[line];
     B[line]=B[guideLine];
     B[guideLine]=tmp;
     return;
}

/**
 * @brief Returns the absolute value of a number.
 * @param number the number whose absolute value will be returned. 
 */
double absValue(double number){
     if (number<0.0)
          number*=-1.0;
     return number;
}


/**
 * @brief Applies a partial guidance.
 * @param U the matrix in which the partial guidance will be applied.
 * @param j the number of the row which would be the guide line if we were 
 *          not using partial guidance.
 */
int applyGuidance(double U[][N], double B[N], int j){
     int i=0;
     double guide = U[j][j];
     int guideLine=j;
     for(i=j+1; i<N; i++){
          if (absValue(guide) < absValue(U[i][j])){
               guide = U[i][j];
               guideLine = i;
          }
     }
     if (j!=guideLine){
          swapLines(U, j, guideLine);
          swapElements(B, j, guideLine);
     }
     return guideLine;
}

/** 
 * @brief Creates the Upper and Lower Matrix from matrix A according LU method using partial guidance.
 * @param L the Lower matrix created by LU method
 * @param U the Upper matrix created by LU method
 */
int calculateLU(double L[][N], double U[][N], double B[N]){
     int i=0;
     int j=0;
     double multiplier=1.0;
     int guideLine;
     for (j=0; j<N; j++){
          //applyGuidance(U,B,j);
          printf("guideLine:%d\n",guideLine);
          for (i=j+1; i<N; i++){
               multiplier=(U[i][j]/U[j][j]);
               L[i][j]= multiplier!=0 ? multiplier : 0;
               updateUrow(U,i,j,-1*multiplier);
          }
          L[j][j] = 1;
     }
     return;
}

/**
 * @brief Copy the content of a matrix inside an other.
 * @param A the matrix which will be coppied.
 * @param B the matrix which will be initialized.
 */
void copyMatrices(double A[][N], double B[][N]){
     int i,j;
     for (i=0; i<N ; i++){
          for (j=0; j<N; j++){
               B[i][j]=A[i][j];
          }
     }
     return;
}

/**
 * @brief 
 * @param
 * @param
 * @param
 */
void findY(double L[][N], double Y[N], double B[N]){
     int i,j;
     double quantity;
     for (i=0; i<N; i++){
          quantity=0;
          for(j=0; j<i; j++){
               quantity+=L[i][j]*Y[j];
          }
          Y[i]=(B[i]-quantity)/L[i][i] ;
     }
     return;
}

void findX(double U[][N], double X[N], double Y[N]){
     int i,j;
     double quantity;
     for (i=N-1; i>=0; i--){
          quantity=0;
          for(j=N-1; j>i; j--){
               quantity+=U[i][j]*X[j];
          }
          X[i]=(Y[i]-quantity)/U[i][i] ;
     }
     return;
}

/**
 * @brief Dummy main
 */
int main(int argc, char* argv[]){

     FILE *myfile;
     double myvariable;
     int i;
     int j;
     myfile=fopen(argv[1], "r");
     fscanf(myfile,"%d",&N);
     printf("\nDimension:%d\n\n",N);
     double A[N][N];
     double B[N];
     for(i = 0; i < N; i++){
          for (j = 0 ; j < N; j++){
               fscanf(myfile,"%lf",&myvariable);
               A[i][j]=myvariable;
          }
     }
     for(i=0; i<N; i++){
          fscanf(myfile,"%lf", &myvariable);
          B[i]=myvariable;
     }
     fclose(myfile);
     
     double L[N][N];
     double U[N][N];
     double X[N];
     double Y[N];

     copyMatrices(A,U);     
     calculateLU(L,U,B);
     findY(L,Y,B);
     findX(U,X,Y);

     printMatrix_2d(A, "A");
     printMatrix_1d(B, "B");
     printMatrix_2d(L, "L");
     printMatrix_2d(U, "U");

     printMatrix_1d(Y, "Y");
     printMatrix_1d(X, "X");

}