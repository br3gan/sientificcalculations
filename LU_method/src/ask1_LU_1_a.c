// TODO epafksimenos pinakas

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define NAME_LENGTH 10
#define MAX_RAND 500

int N;

double absValue(double number){
     if (number<0.0)
          number*=-1.0;
     return number;
}

void printMatrix_2(double A[][N], char name[NAME_LENGTH]){
     int i,j;
     printf("%s Matrix:\n",name);
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               printf("%8.3f ", A[i][j]);
          }
          printf("\n");
     }
     printf("\n");
     return;
}

void printMatrix_2_reverse(double A[][N], char name[NAME_LENGTH]){
     int i,j;
     printf("%s Matrix:\n",name);
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               printf("%8.3f ", A[j][i]);
          }
          printf("\n");
     }
     printf("\n");
     return;
}

void printL(double LU[][N]){
     
     int i,j;
     printf("L Matrix:\n");
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               if (i==j)
                    printf("%8.3f ", 1.0);
               else if (j < i)
                    printf("%8.3f ", LU[i][j]);
               else
                    printf("%8.3f ", 0.0);
               
          }
          printf("\n");
     }
     printf("\n");
     return;
}

void printU(double LU[][N]){
     
     int i,j;
     printf("U Matrix:\n");
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               if (j >= i)
                    printf("%8.3f ", LU[i][j]);
               else
                    printf("%8.3f ", 0.0);
               
          }
          printf("\n");
     }
     printf("\n");
     return;
}
/**
 * @brief Prints a one-dimensioned Matrix.
 * @param A The matrix which is going to be printed.
 * @param name The name of the matrix for elegand output.
 */
void printMatrix_1(double A[N], char name[NAME_LENGTH]){
     int i;
     printf("%s Matrix:\n",name);
     for (i=0; i<N; i++){
          printf("%8.3f ", A[i]);
     }
     printf("\n\n");
     return;
}


void printMatrix_1_int(int A[N], char name[NAME_LENGTH]){
     int i;
     printf("%s Matrix:\n",name);
     for (i=0; i<N; i++){
          printf("%d ", A[i]);
     }
     printf("\n\n");
     return;
}

void printPermutation(int Pivotes[N]){
     int i,j;
     printf("Permutation Matrix:\n");
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               if (Pivotes[j]==i){
                    printf("%8d ", 1);
                    continue;
               }
               printf("%8d ", 0);     
          }
          printf("\n");
     }
     printf("\n");
     return;
}

void initializeArray_1(double A[N]){
     int i;
     for(i=0; i<N; i++){
          A[i]=0;
     }
     return;
}

void initializeIncrArray_1_int(int A[N]){
     int i;
     for(i=0; i<N; i++){
          A[i]=i;
     }
     return;
}

void initializeArray_2(double A[][N]){
     int i,j;
     for (i=0; i<N; i++){
          for(j=0; j<N; j++){
               A[i][j]=0;
          }
     }
     return;
}

void findUrow(double A[][N], double U[][N], double L[][N], int r, int Pivotes[N]){
     int p=0, j=0;
     double sum;
     for(p=r; p<N; p++){
          sum=0;
          for(j=0; j<r; j++){
               sum+=L[r][j]*U[j][p];
          }
          U[r][p]=A[Pivotes[r]][p]-sum;
     }
}

void findLcolumn(double A[][N], double U[][N], double L[][N], double S[N], int r){
     int p=0;
     for(p=r+1; p<N; p++){
          L[p][r]=S[p]/U[r][r];
     }
}

void updatePivotingVector(double A[][N], double L[][N], double U[][N], double S[N], int Pivotes[N], int round){
     
     int i,j;
     int pivotingLine;
     
     double sum;
     for (i=round; i<N; i++){
          sum=0;
          for(j=0; j<round; j++){
               sum+=L[i][j]*U[j][round];
          }
          S[i]=A[Pivotes[i]][round]-sum;
     }
     
     double max=S[round];
     int maxi=round;
     for (i=round+1; i<N; i++){
          if (absValue(max) < absValue(S[i]) ){
               maxi=i;
               max=S[i];
          }
     }
     
     pivotingLine=maxi;
     
     if (pivotingLine!=Pivotes[round]){
          int tmp=Pivotes[round];
          Pivotes[round] = Pivotes[pivotingLine];
          Pivotes[pivotingLine]=tmp;
          
          
          double s=S[round];
          S[round] = S[pivotingLine];
          S[pivotingLine]=s;
          
          double l;
          for (i=0; i<round; i++){
               l=L[round][i];
               L[round][i]=L[pivotingLine][i];
               L[pivotingLine][i]=l;
          }
     }
     
     return;
}

void calculateLU(double A[N][N], double L[N][N], double U[N][N], double S[N], int Pivotes[N]){
     
     int i;
     for (i=0; i<N; i++){
          updatePivotingVector(A,L,U,S,Pivotes,i);
          findUrow(A,U,L,i,Pivotes);
          findLcolumn(A,U,L,S,i);
     }
     return;
}

void findY(double L[][N], double Y[N], double B[N], int Pivotes[N]){
     int i,j;
     double quantity;
     for (i=0; i<N; i++){
          quantity=0;
          for(j=0; j<i; j++){
               if(i==j)
                    quantity+=Y[j];
               else
                    quantity+=L[i][j]*Y[j];
          }
          Y[i]=(B[Pivotes[i]]-quantity);
     }
     return;
}

void findX(double U[][N], double X[N], double Y[N]){
     int i,j;
     double quantity;
     for (i=N-1; i>=0; i--){
          quantity=0;
          for(j=N-1; j>i; j--){
               quantity+=U[i][j]*X[j];
          }
          X[i]=(Y[i]-quantity)/U[i][i] ;
     }
     return;
}

double randomIndex(){
    return rand(); 
}

int main(int argc, char* argv[]){
    
     int readFromFile = 0;
     int readFromUser = 0;
     int random = 0;
     FILE *myfile;
     
     if (argc > 1){
          if ( strcmp(argv[1],"--user") == 0 || strcmp(argv[1],"-u") == 0 )
               readFromUser = 1;
          else if ( strcmp(argv[1],"--file") == 0 || strcmp(argv[1],"-f") == 0 )
               readFromFile = 1;
          else if ( strcmp(argv[1],"--random") == 0 || strcmp(argv[1],"-r") == 0 )
               random = 1;
          else{
               printf("Invalid arguments\n");
               return -1;
          }
     }
     else{
          printf("Please give an option\n");
          printf("--file / -f [path/to/file] : to read from file\n");
          printf("--user / -u : to read from user\n");
          printf("--random / -r [dimension] : to fill a random Matrix\n");
          return 0;
     }
     
     if(readFromFile){
          myfile=fopen(argv[2], "r");
          if (myfile == NULL){
               printf("Give file path\n");
               return -1;
          }
          fscanf(myfile,"%d",&N);
     }
     else if(random){
          if (argc<3){
               printf("Give Dimension : ");
               scanf("%d",&N);
          }
          else{
               N=atoi(argv[2]);
               printf("Dimension : %d\n", N);
          }
     }
     if (random){
          srand(time(NULL));
     }
     double A[N+1][N];
     int i;
     int j;
     double myvariable;
     
     if (readFromUser){
          printf("Give A matrix index\n");
     }

     for(i = 0; i < N; i++){
          for (j = 0 ; j < N; j++){
               if(readFromFile)
                    fscanf(myfile,"%lf",&myvariable);
               else if(readFromUser){
                    printf(" A[%d][%d] = ",i,j);
                    scanf("%lf",&myvariable);
                    if (j==N-1) printf("\n");
               }
               else if(random){
                    myvariable = (double)(rand()%MAX_RAND);
               }
               A[i][j]=myvariable;
          }
     }
     
     if (readFromUser){
          printf("Give B matrix index\n");
     }
     
     for(i=0; i<N; i++){
          if(readFromFile){
               fscanf(myfile,"%lf", &myvariable);
          }
          else if(readFromUser){
               printf(" B[%d] = ",i);
               scanf("%lf",&myvariable);
               if (i==N-1) printf("\n");
          }
          else if(random){
               myvariable = (double)(rand()%MAX_RAND);
          }
          A[N][i]=myvariable;
     }
     
     if(readFromFile)
          fclose(myfile);
     
     double LU[N][N];
     double Y[N];
     double X[N];
     double S[N];
     int Pivotes[N];
     
     initializeArray_2(LU);
     initializeArray_1(Y);
     initializeArray_1(X);
     initializeArray_1(S);
     initializeIncrArray_1_int(Pivotes);
    
     calculateLU(A,LU,LU,S,Pivotes);
     findY(LU,Y,A[N],Pivotes);
     findX(LU,X,Y);
    
     printMatrix_2(A,"A");
     printMatrix_1(A[N],"B");
     printL(LU);
     printU(LU);
     printPermutation(Pivotes);

     double Inverse[N][N];
     double I[N][N];
     for (i=0; i<N; i++){
          findY(LU,Y,I[i],Pivotes);
          findX(LU,Inverse[i],Y);
     }
  
     printMatrix_1(X,"X");
          
     return 0;
}